<?php
if($mainfile!=TRUE) die("Plik mozna tylko includowac!");

$idTrasy = $_GET['trasa'];

function nazwa(&$conn, $id){
	$query = "SELECT nazwa FROM stacje WHERE ID=" . $id . " LIMIT 1";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	return $row['nazwa'];
}

function ilosc_przystankow(&$conn){
	$query = "SELECT * FROM przystanki WHERE ID_trasy=" . $_GET['trasa'];
	$result = mysqli_query($conn, $query);
	return mysqli_num_rows($result);
}


if(isset($_GET['add'])){
	$ilosc = 1 + ilosc_przystankow($conn);
	$query = "INSERT INTO przystanki values('', '".$_GET['trasa']."', '" . $ilosc . "', '00:00', '0', '".$_GET['add']."')";
	if(mysqli_query($conn, $query)){
		echo "<h2 style=\"color:green\">Dodano przystanek!</h2>" . PHP_EOL;
	}else{
		echo "<h2 style=\"color:red\">Nie dodano!</h2>" . PHP_EOL;
		echo mysqli_error($conn);
	}
}

if(isset($_POST['zapisz'])){
	$query = "UPDATE przystanki SET czas_przyjazdu='" . $_POST['czas'] . "', cena='" . $_POST['cena'] . "' WHERE ID='" . $_POST['id'] . "'";
	if(mysqli_query($conn, $query)){
		echo "<h2 style=\"color:green\">Zapisano!</h2>" . PHP_EOL;
	}
}


if(isset($_GET['edit'])){
	$query = "SELECT * FROM przystanki WHERE ID = " . $_GET['edit'] . " LIMIT 1";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	echo "<h2>Edytuj przystanek \"" . nazwa($conn, $row['ID_stacji']) . "\":</h2>" . PHP_EOL;
	echo "<form method=\"post\">Czas przyjazdu:<input type=\"text\" name=\"czas\" value=\"" . $row['czas_przyjazdu'] . "\"> Cena:<input type=\"text\" name=\"cena\" value=\"" . $row['cena'] . "\"> <input type=\"hidden\" name=\"id\" value=\"" . $row['ID'] . " \"> <input type=\"submit\" name=\"zapisz\" value=\"Zapisz\"></form> " . PHP_EOL;
}


if(isset($_GET['up'])){
	if($_GET['kol']==1){
		echo "<h2 style=\"color:red\">Operacja niedozwolona!</h2>" . PHP_EOL;
	}else{
		$prev = $_GET['kol'] - 1;
		
		$query = "UPDATE przystanki set kolejnosc='" . $_GET['kol'] . "' WHERE ID_trasy='" . $_GET['trasa'] . "' AND kolejnosc='" . $prev . "'";
		mysqli_query($conn, $query);
		
		$query = "UPDATE przystanki set kolejnosc='" . $prev . "' WHERE ID_trasy='" . $_GET['trasa'] . "' AND ID='" . $_GET['up'] . "'";
		mysqli_query($conn, $query);
		
		echo "<h2 style=\"color:green\">Zmieniono kolejność</h2>" . PHP_EOL;
	}
}

if(isset($_GET['down'])){
	if($_GET['kol']==ilosc_przystankow($conn)){
		echo "<h2 style=\"color:red\">Operacja niedozwolona!</h2>" . PHP_EOL;
	}else{
		$next = $_GET['kol'] + 1;
		
		$query = "UPDATE przystanki set kolejnosc='" . $_GET['kol'] ."' WHERE ID_trasy='" . $_GET['trasa'] . "' AND kolejnosc='" . $next . "'";
		mysqli_query($conn, $query);
		
		$query = "UPDATE przystanki set kolejnosc='" . $next ."' WHERE ID_trasy='" . $_GET['trasa'] . "' AND ID='" . $_GET['down'] . "'";
		mysqli_query($conn, $query);
		
		echo "<h2 style=\"color:green\">Zmieniono kolejność</h2>" . PHP_EOL;
	}
}

$query = "SELECT * FROM przystanki WHERE ID_trasy=" . $idTrasy . " ORDER BY kolejnosc";
$result = mysqli_query($conn, $query);

if(mysqli_num_rows($result)>0){
	echo "<table><tr><td><b>Kolejnosc</b></td><td><b>Przystanek</b></td><td><b>Czas przyjazdu</b></td><td><b>Koszt przejazdu</b></td><td><b>Opcje</b></td></tr>" . PHP_EOL;
	while($row = mysqli_fetch_assoc($result)){
		echo "<tr><td><a style=\"color:green\" href=\"?menu=przystanki&trasa=" . $_GET['trasa'] . "&up=" . $row['ID'] . "&kol=" . $row['kolejnosc'] . "\">▲</a>" . $row['kolejnosc'] . "<a style=\"color:green\" href=\"?menu=przystanki&trasa=" . $_GET['trasa'] . "&down=" . $row['ID'] . "&kol=" . $row['kolejnosc'] . "\">▼</a></td><td>" . nazwa($conn, $row['ID_stacji']) . "</td><td>" . $row['czas_przyjazdu'] . "</td><td> " . $row['cena'] . "zł</td><td><a href=\"?menu=przystanki&trasa=" . $_GET['trasa'] . "&edit=" . $row['ID'] . "\">Edytuj</a></td></tr>" . PHP_EOL;
	}
	echo "</table>" . PHP_EOL;
}else{
	echo "Brak wyników" . PHP_EOL;
}




echo "<h2>Dodaj przystanek do trasy:</h2>" . PHP_EOL;
echo "<table><tr><td>Nazwa przystanku</td><td>Ulica</td><td>Numer</td><td>Opcje</td></tr>" . PHP_EOL;

$query = "SELECT * FROM stacje ORDER BY nazwa";
$result = mysqli_query($conn, $query);
if(mysqli_num_rows($result)>0){
	while($row = mysqli_fetch_assoc($result)){
		echo "<tr><td>" . $row['nazwa'] . "</td><td>" . $row['ulica'] . "</td><td>" . $row['numer'] . "</td><td><a href=\"?menu=przystanki&trasa=".$_GET['trasa']."&add=".$row['ID']."\">Dodaj</a></td></tr>" . PHP_EOL;
	}
}
echo "</table>" . PHP_EOL;

?>