<?php
session_start();
require_once("conf.php");
$mainfile = TRUE;

$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


mysqli_query($conn, "SET NAMES `utf8` COLLATE `utf8_general_ci`"); 
mysqli_query($conn, "SET CHARSET utf8");



echo "<!DOCTYPE html>
<html>
<head>
	<title>Projekt zespołowy</title>
	<meta charset=\"utf-8\"  />
</head>
<body>";

if($_SESSION['zalogowany']==TRUE){ //jesli jestesmy zalogowani
	if(isset($_GET['wyloguj'])){
		$_SESSION['zalogowany'] = FALSE;
		echo "<h2>Wylogowano! Za chwilę nastąpi przekierowanie...</h2> <script type=\"text/javascript\">setTimeout(function(){  window.location.href = \"http://trans.dziecielski.pl/admin/\";},5000);</script>";
	}
	echo "<div class=\"menu\"> <a href=\"?menu=trasy\">TRASY</a> | <a href=\"?menu=stacje\">STACJE</a> | <a href=\"?menu=klienci\">KLIENCI</a> | <a href=\"?wyloguj\">WYLOGUJ</a> </div>"; // MENU 
	
	echo "<div class=\"content\">";
	switch($_GET['menu']){
		case "trasy":
			require_once("trasy.php");
			break;
		case "stacje":
			require_once("stacje.php");
			break;
		case "przystanki":
			require_once("przystanki.php");
			break;
		case "klienci":
			require_once("klienci.php");
			break;
	}
	echo "</div>";
	
} else { // jesli nie jestesmy zalogowani
	if(isset($_POST['user'])){ // jesli przeslano formularz
		if($_POST['user']==$user && $_POST['pass']==$pass){ // jesli dane sie zgadzaja
			$_SESSION['zalogowany'] = TRUE;
			echo "<h2>Zalogowano poprawnie! Za chwilę nastąpi przekierowanie...</h2> <script type=\"text/javascript\">setTimeout(function(){  window.location.href = \"http://trans.dziecielski.pl/admin/\";},5000);</script>";
		}else{ // jesli dane sa niepoprawne
			echo "<h2>Niepoprawne dane!</h2>";
		}
	}
	echo "<form method=\"post\"> Login:<input type=\"text\" name=\"user\"> Haslo:<input type=\"password\" name=\"pass\"> <input type=\"submit\" value=\"Zaloguj\"> </form>";
}



echo "</body></html>";
?>