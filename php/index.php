<?php
session_start();
require_once("conf.php");
$mainfile = TRUE;

$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


mysqli_query($conn, "SET NAMES `utf8` COLLATE `utf8_general_ci`"); 
mysqli_query($conn, "SET CHARSET utf8");



echo "<!DOCTYPE html>
<html>
<head>
	<title>Projekt zespołowy</title>
	<meta charset=\"utf-8\"  />
	<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
	<link rel=\"stylesheet\" href=\"/resources/demos/style.css\">
	<script src=\"https://code.jquery.com/jquery-1.12.4.js\"></script>
	<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
	<script>
	$( function() {
		$( \"#datepicker\" ).datepicker({dateFormat: \"yy-mm-dd\", minDate: 0});
	} );
	</script>
</head>
<body>";



function trasy(&$conn, $id1, $id2){
	//SELECT t.* FROM (SELECT * FROM `przystanki` WHERE ID_stacji='3') t
	$ilosc = 0;
	$i=0;
	$j=0;
	
	$query = "SELECT * FROM przystanki WHERE ID_stacji='".$id1."'";
	$result1 = mysqli_query($conn, $query);
	while($row1 = mysqli_fetch_assoc($result1)){
		$trasy1[$i++] = $row1['ID_trasy'];
	}
	
	$query = "SELECT * FROM przystanki WHERE ID_stacji='".$id2."'";
	$result2 = mysqli_query($conn, $query);
	while($row2 = mysqli_fetch_assoc($result2)){
		$trasy2[$j++] = $row2['ID_trasy'];
	}
	
	for($i=0; $i<count($trasy1); $i++){
		for($j=0; $j<count($trasy2); $j++){
			if($trasy1[$i]==$trasy2[$j]){
				$trasy[$ilosc++] = $trasy1[$i];
			}
		}
	}
	
	if($ilosc==0) $trasy = null;
	
	return $trasy;
}

	
function nazwaStacji(&$conn, $id){
	$query = "SELECT * FROM stacje WHERE ID='".$id."' LIMIT 1";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	return $row['nazwa'];
}

function relacja(&$conn, $id){
	$query = "SELECT * FROM trasy WHERE ID='".$id."' LIMIT 1";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	return $row['p_pocz'] . " --> " . $row['p_konc'] ;
}

function kolejnosc(&$conn, $trasa, $stacja){
	$query = "SELECT * FROM przystanki WHERE ID_trasy='".$trasa."' AND ID_stacji='".$stacja."' LIMIT 1";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	return $row['kolejnosc'];
}

function cena(&$conn, $trasa, $from, $to){
	$cena = 0;
	
	$query = "SELECT * FROM przystanki WHERE ID_trasy='".$trasa."' AND kolejnosc>='". kolejnosc($conn, $trasa, $from) ."' AND kolejnosc<'". kolejnosc($conn, $trasa, $to) ."'";
	$result = mysqli_query($conn, $query);
	while($row = mysqli_fetch_assoc($result)){
		$cena = $cena + $row['cena'];
	}
	return $cena;
}

function wolneMiejsca(&$conn, $trasa, $from, $to, $data, $wolne){
	$query = "SELECT count(*) as ile FROM bilety WHERE ID_trasy='".$trasa."' AND p_konc>'". kolejnosc($conn, $trasa, $from) ."' AND p_pocz<'". kolejnosc($conn, $trasa, $to) ."' AND data_podrozy='".$data."'";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	return $wolne - $row['ile'];
}

function zajeteMiejsca(&$conn, $trasa, $from, $to, $data){
	$query = "SELECT count(*) as ile FROM bilety WHERE ID_trasy='".$trasa."' AND p_pocz>='". kolejnosc($conn, $trasa, $from) ."' AND p_konc<'". kolejnosc($conn, $trasa, $to) ."' AND data_podrozy='".$data."'";
	//echo $query . PHP_EOL;
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	return $row['ile'];
}

function idKlienta(&$conn, $email){
	$query = "SELECT * FROM klient WHERE email='".$email."' LIMIT 1";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	if(mysqli_num_rows($result)){
		return $row['ID'];
	}
	return 0;
}

echo "<h2>Formularz zamawiania biletu:</h2>" . PHP_EOL;



if(isset($_POST['zaplac'])){
	$idKlienta = idKlienta($conn, $_POST['email']);
	if(!$idKlienta){
		$query = "INSERT INTO klient VALUES('', '". $_POST['imie'] ."', '". $_POST['nazwisko'] ."', '". $_POST['email'] ."', '". $_POST['numer'] ."')";
		mysqli_query($conn, $query);
		$idKlienta = mysqli_insert_id($conn);
	}
	
	$idbiletu = "B:" . zajeteMiejsca($conn, $_POST['trasa'], $_POST['from'], $_POST['to'], $_POST['data']) . "K:" . $idKlienta . "T:" .$_POST['trasa'] . "F:" . $_POST['from'] . "T:" . $_POST['to'];
	$query = "INSERT INTO bilety VALUES('".$_POST['trasa']."', '".$idbiletu."', '".$idKlienta."', '" . kolejnosc($conn, $_POST['trasa'], $_POST['from']) . "', '" . kolejnosc($conn, $_POST['trasa'], $_POST['to']) . "', '".$_POST['data']."', '". date('Y-m-d') ."', '" . cena($conn, $_POST['trasa'], $_POST['from'], $_POST['to']) . "')";
	if(mysqli_query($conn, $query)){
		echo "<h2 style=\"color:green\">Bilet kupiony! ID biletu:".$idbiletu."</h2>" . PHP_EOL;
	}
}


if(isset($_POST['dalej'])){
	
	$trasy = trasy($conn, $_POST['from'], $_POST['to']);

	if(count($trasy)==null){
		echo "<h2 style=\"color:red\">Nie ma trasy łączącej wybrane miasta! <a href=\"http://trans.dziecielski.pl/\">Powrót</a></h2>" . PHP_EOL;
	}else{
	
		// jesli znajdzie sie taka trasa
		$query = "SELECT * FROM klient WHERE email='" . $_POST['email'] . "' LIMIT 1";
		$result = mysqli_query($conn, $query);
		$row = mysqli_fetch_assoc($result);
		
		echo "<h3>Bilet z ". nazwaStacji($conn, $_POST['from']) ." do ". nazwaStacji($conn, $_POST['to']) ."</h3>";
		echo "<form method=\"post\">  Imię<input type=\"text\" name=\"imie\" value=\"".$row['imię']."\"><br /> Nazwisko<input type=\"text\" name=\"nazwisko\" value=\"".$row['nazwisko']."\"><br /> Numer telefonu:<input type=\"text\" name=\"numer\" value=\"".$row['numer-telefonu']."\"><br /> <input type=\"hidden\" name=\"email\" value=\"".$_POST['email']."\"> <input type=\"hidden\" name=\"from\" value=\"".$_POST['from']."\"> <input type=\"hidden\" name=\"to\" value=\"".$_POST['to']."\"> <input type=\"hidden\" name=\"data\" value=\"".$_POST['data']."\">" . PHP_EOL;
		for($i=0; $i<count($trasy); $i++){
			echo "<input type=\"radio\" name=\"trasa\" value=\"". $trasy[$i] ."\">Cena ". cena($conn, $trasy[$i], $_POST['from'], $_POST['to']) ."zł, relacja (". relacja($conn, $trasy[$i]) ."), liczba miejsc ". wolneMiejsca($conn, $trasy[$i], $_POST['from'], $_POST['to'], $_POST['data'], $iloscMiejsc) ." <br /> " . PHP_EOL;
		}
		
		echo "<input type=\"submit\" name=\"zaplac\" value=\"Zapłać\">" . PHP_EOL;
	}
}else{
	
echo "<form method=\"post\"><table><tr><td>Podaj adres e-mail:</td><td><input type=\"text\" name=\"email\"></td></tr>";

$query = "SELECT * FROM stacje ORDER BY nazwa";
$result1 = mysqli_query($conn, $query);
$result2 = mysqli_query($conn, $query);

echo "<tr><td>Skąd:</td><td><select name=\"from\">";
	while($row = mysqli_fetch_assoc($result1)){
		echo "<option value=\"" . $row['ID'] . "\">" . $row['nazwa'] . " " . $row['ulica'] . " " . $row['numer'] . "</option>" . PHP_EOL;
	}
echo "</select></td></tr>";

echo "<tr><td>Dokąd:</td><td><select name=\"to\">";
	while($row = mysqli_fetch_assoc($result2)){
		echo "<option value=\"" . $row['ID'] . "\">" . $row['nazwa'] . " " . $row['ulica'] . " " . $row['numer'] . "</option>" . PHP_EOL;
	}
echo "</select></td></tr>". PHP_EOL;
echo "<tr><td>Kiedy:</td><td><input type=\"text\" id=\"datepicker\" name=\"data\"> </td></tr>" . PHP_EOL;

echo "<tr><td></td><td><input type=\"submit\" name=\"dalej\" value=\"Dalej\"></td></table></form>". PHP_EOL;
}

echo "</body></html>". PHP_EOL;


?>