-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 22 Cze 2017, 16:19
-- Wersja serwera: 10.1.18-MariaDB
-- Wersja PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `szakir_bus`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bilety`
--

CREATE TABLE `bilety` (
  `ID_trasy` int(11) NOT NULL,
  `ID_biletu` varchar(30) NOT NULL,
  `ID_klienta` int(11) NOT NULL,
  `p_pocz` int(11) NOT NULL,
  `p_konc` int(11) NOT NULL,
  `data_podrozy` date NOT NULL,
  `data_zakupu` date NOT NULL,
  `cena` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `bilety`
--

INSERT INTO `bilety` (`ID_trasy`, `ID_biletu`, `ID_klienta`, `p_pocz`, `p_konc`, `data_podrozy`, `data_zakupu`, `cena`) VALUES
(1, '1_wroc_war_280517', 1, 2, 4, '2017-05-28', '2017-05-24', 42),
(2, '2_op_kr_08062017', 2, 9, 10, '2017-06-08', '2017-06-01', 28),
(10, '3_10 - 2 - 8', 3, 1, 3, '2017-06-23', '2017-06-22', 8),
(2, 'B:0K:3T:2F:2T:11', 3, 4, 7, '2017-06-23', '2017-06-22', 72),
(10, 'B:1K:2T:10F:2T:8', 2, 1, 3, '2017-06-23', '2017-06-22', 8),
(10, 'B:1K:3T:10F:2T:8', 3, 1, 3, '2017-06-23', '2017-06-22', 8),
(10, 'bilet_wro_rze', 2, 1, 2, '2017-06-23', '2017-06-23', 55);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `ID` int(11) NOT NULL,
  `imię` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `nazwisko` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `numer-telefonu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`ID`, `imię`, `nazwisko`, `email`, `numer-telefonu`) VALUES
(1, 'Antoni', 'Kwiatkowski', 'kwiatkowski.antoni@gmail.com', 608777915),
(2, 'Justyna', 'Olejnik', 'justynaolejnik94@gmail.com', 723884698),
(3, 'Martin', 'Dzięcielski ', 'm_zaba@wp.pl', 796765776);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przystanki`
--

CREATE TABLE `przystanki` (
  `ID` int(11) NOT NULL,
  `ID_trasy` int(11) NOT NULL,
  `kolejnosc` int(11) NOT NULL,
  `czas_przyjazdu` varchar(5) NOT NULL,
  `cena` double NOT NULL,
  `ID_stacji` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `przystanki`
--

INSERT INTO `przystanki` (`ID`, `ID_trasy`, `kolejnosc`, `czas_przyjazdu`, `cena`, `ID_stacji`) VALUES
(1, 1, 1, '6:00', 18, 1),
(2, 1, 2, '8:50', 20, 2),
(3, 1, 3, '10:55', 22, 3),
(4, 1, 4, '13:10', 16, 4),
(5, 1, 5, '16:15', 0, 5),
(6, 2, 1, '6:00', 17, 6),
(7, 2, 2, '7:20', 17, 7),
(8, 2, 3, '8:45', 17, 12),
(9, 2, 4, '11:25', 28, 2),
(10, 2, 5, '13:40', 22, 8),
(11, 2, 6, '15:55', 22, 10),
(12, 2, 7, '17:50', 0, 11),
(18, 6, 1, '3:45', 33, 14),
(19, 6, 2, '7:55', 18, 3),
(20, 6, 3, '10:00', 16, 8),
(21, 6, 4, '11:05', 0, 10),
(29, 10, 1, '00:00', 3, 2),
(30, 10, 2, '00:00', 5, 9),
(31, 10, 3, '00:00', 7, 8),
(32, 10, 4, '00:00', 9, 10),
(33, 10, 5, '00:00', 0, 11),
(34, 11, 1, '00:00', 4, 2),
(35, 11, 2, '00:00', 8, 9),
(36, 11, 3, '00:00', 12, 8),
(37, 11, 4, '00:00', 0, 10),
(38, 12, 1, '00:00', 0, 14),
(39, 12, 2, '00:00', 0, 8),
(40, 12, 3, '00:00', 0, 10),
(41, 12, 4, '00:00', 0, 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stacje`
--

CREATE TABLE `stacje` (
  `ID` int(11) NOT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `ulica` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `numer` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stacje`
--

INSERT INTO `stacje` (`ID`, `nazwa`, `ulica`, `numer`) VALUES
(1, 'Zgorzelec', 'ul. gen. Józefa Bema', 1),
(2, 'Wrocław', 'ul. Joanitów', 13),
(3, 'Łódź', 'ul. Smutna', 28),
(4, 'Warszawa Centralna', 'ul. Aleje Jerozolimskie', 54),
(5, 'Białystok', 'ul. Bohaterów Monte Cassino', 10),
(6, 'Szczecin', 'pl. Grodnicki', 1),
(7, 'Gorzów Wielkopolski', 'ul. Dworcowa', 10),
(8, 'Katowice', 'ul. Skargi', 1),
(9, 'Opole', 'ul. Marii Rodziewiczówny', 1),
(10, 'Kraków', 'ul. Bosacka', 18),
(11, 'Rzeszów', 'al. Wyzwolenia', 6),
(12, 'Zielona Góra', 'ul. Jana z Kolna', 2),
(13, 'Kalisz', 'ul. Dworcowa', 17),
(14, 'Gdańsk', 'ul. Kolejowa', 8),
(15, 'Bydgoszcz', 'ul. Kościuszki', 13),
(16, 'Konin', 'ul. Paderewskiego', 15);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `trasy`
--

CREATE TABLE `trasy` (
  `ID` int(11) NOT NULL,
  `p_pocz` varchar(20) NOT NULL,
  `p_konc` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `trasy`
--

INSERT INTO `trasy` (`ID`, `p_pocz`, `p_konc`) VALUES
(1, 'Zgorzelec', 'Białystok'),
(2, 'Szczecin', 'Rzeszów'),
(6, 'Gdańsk', 'Kraków'),
(10, 'Wrocław', 'Rzeszów'),
(11, 'Wrocław', 'Kraków'),
(12, 'Gdańsk', 'Rzeszów');

--
-- Wyzwalacze `trasy`
--
DELIMITER $$
CREATE TRIGGER `usun_przystanki` BEFORE DELETE ON `trasy` FOR EACH ROW DELETE FROM przystanki WHERE ID_trasy = OLD.ID
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `v_bilety`
-- (See below for the actual view)
--
CREATE TABLE `v_bilety` (
`ID_trasy` int(11)
,`ID_biletu` varchar(30)
,`ID_klienta` int(11)
,`p_pocz` int(11)
,`p_konc` int(11)
,`data_podrozy` date
,`data_zakupu` date
,`cena` double
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `v_dane`
-- (See below for the actual view)
--
CREATE TABLE `v_dane` (
`email` varchar(50)
,`ID_biletu` varchar(30)
,`ID_trasy` int(11)
,`p_pocz` int(11)
,`p_konc` int(11)
,`data_podrozy` date
,`cena` double
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `v_klient`
-- (See below for the actual view)
--
CREATE TABLE `v_klient` (
`ID` int(11)
,`imię` varchar(30)
,`nazwisko` varchar(50)
,`email` varchar(50)
,`numer-telefonu` int(11)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `v_przystanki`
-- (See below for the actual view)
--
CREATE TABLE `v_przystanki` (
`ID` int(11)
,`ID_trasy` int(11)
,`kolejnosc` int(11)
,`czas_przyjazdu` varchar(5)
,`cena` double
,`ID_stacji` int(11)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `v_stacje`
-- (See below for the actual view)
--
CREATE TABLE `v_stacje` (
`ID` int(11)
,`nazwa` varchar(50)
,`ulica` varchar(50)
,`numer` int(5)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `v_trasy`
-- (See below for the actual view)
--
CREATE TABLE `v_trasy` (
`ID` int(11)
,`p_pocz` varchar(20)
,`p_konc` varchar(20)
);

-- --------------------------------------------------------

--
-- Struktura widoku `v_bilety`
--
DROP TABLE IF EXISTS `v_bilety`;

CREATE ALGORITHM=UNDEFINED DEFINER=`szakir_bus`@`localhost` SQL SECURITY DEFINER VIEW `v_bilety`  AS  select `bilety`.`ID_trasy` AS `ID_trasy`,`bilety`.`ID_biletu` AS `ID_biletu`,`bilety`.`ID_klienta` AS `ID_klienta`,`bilety`.`p_pocz` AS `p_pocz`,`bilety`.`p_konc` AS `p_konc`,`bilety`.`data_podrozy` AS `data_podrozy`,`bilety`.`data_zakupu` AS `data_zakupu`,`bilety`.`cena` AS `cena` from `bilety` ;

-- --------------------------------------------------------

--
-- Struktura widoku `v_dane`
--
DROP TABLE IF EXISTS `v_dane`;

CREATE ALGORITHM=UNDEFINED DEFINER=`szakir_bus`@`localhost` SQL SECURITY DEFINER VIEW `v_dane`  AS  select `klient`.`email` AS `email`,`bilety`.`ID_biletu` AS `ID_biletu`,`bilety`.`ID_trasy` AS `ID_trasy`,`bilety`.`p_pocz` AS `p_pocz`,`bilety`.`p_konc` AS `p_konc`,`bilety`.`data_podrozy` AS `data_podrozy`,`bilety`.`cena` AS `cena` from (`bilety` join `klient`) ;

-- --------------------------------------------------------

--
-- Struktura widoku `v_klient`
--
DROP TABLE IF EXISTS `v_klient`;

CREATE ALGORITHM=UNDEFINED DEFINER=`szakir_bus`@`localhost` SQL SECURITY DEFINER VIEW `v_klient`  AS  select `klient`.`ID` AS `ID`,`klient`.`imię` AS `imię`,`klient`.`nazwisko` AS `nazwisko`,`klient`.`email` AS `email`,`klient`.`numer-telefonu` AS `numer-telefonu` from `klient` ;

-- --------------------------------------------------------

--
-- Struktura widoku `v_przystanki`
--
DROP TABLE IF EXISTS `v_przystanki`;

CREATE ALGORITHM=UNDEFINED DEFINER=`szakir_bus`@`localhost` SQL SECURITY DEFINER VIEW `v_przystanki`  AS  select `przystanki`.`ID` AS `ID`,`przystanki`.`ID_trasy` AS `ID_trasy`,`przystanki`.`kolejnosc` AS `kolejnosc`,`przystanki`.`czas_przyjazdu` AS `czas_przyjazdu`,`przystanki`.`cena` AS `cena`,`przystanki`.`ID_stacji` AS `ID_stacji` from `przystanki` ;

-- --------------------------------------------------------

--
-- Struktura widoku `v_stacje`
--
DROP TABLE IF EXISTS `v_stacje`;

CREATE ALGORITHM=UNDEFINED DEFINER=`szakir_bus`@`localhost` SQL SECURITY DEFINER VIEW `v_stacje`  AS  select `stacje`.`ID` AS `ID`,`stacje`.`nazwa` AS `nazwa`,`stacje`.`ulica` AS `ulica`,`stacje`.`numer` AS `numer` from `stacje` ;

-- --------------------------------------------------------

--
-- Struktura widoku `v_trasy`
--
DROP TABLE IF EXISTS `v_trasy`;

CREATE ALGORITHM=UNDEFINED DEFINER=`szakir_bus`@`localhost` SQL SECURITY DEFINER VIEW `v_trasy`  AS  select `trasy`.`ID` AS `ID`,`trasy`.`p_pocz` AS `p_pocz`,`trasy`.`p_konc` AS `p_konc` from `trasy` ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `bilety`
--
ALTER TABLE `bilety`
  ADD PRIMARY KEY (`ID_biletu`),
  ADD KEY `ID_trasy` (`ID_trasy`),
  ADD KEY `ID_klienta` (`ID_klienta`),
  ADD KEY `p_pocz` (`p_pocz`),
  ADD KEY `p_konc` (`p_konc`),
  ADD KEY `p_pocz_2` (`p_pocz`),
  ADD KEY `p_konc_2` (`p_konc`);

--
-- Indexes for table `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `przystanki`
--
ALTER TABLE `przystanki`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_stacji` (`ID_stacji`),
  ADD KEY `ID_trasy` (`ID_trasy`);

--
-- Indexes for table `stacje`
--
ALTER TABLE `stacje`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `unikalny` (`nazwa`);

--
-- Indexes for table `trasy`
--
ALTER TABLE `trasy`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `klient`
--
ALTER TABLE `klient`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `przystanki`
--
ALTER TABLE `przystanki`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT dla tabeli `stacje`
--
ALTER TABLE `stacje`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT dla tabeli `trasy`
--
ALTER TABLE `trasy`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `bilety`
--
ALTER TABLE `bilety`
  ADD CONSTRAINT `bilety_ibfk_1` FOREIGN KEY (`ID_trasy`) REFERENCES `trasy` (`ID`),
  ADD CONSTRAINT `bilety_ibfk_2` FOREIGN KEY (`ID_klienta`) REFERENCES `klient` (`ID`),
  ADD CONSTRAINT `bilety_ibfk_3` FOREIGN KEY (`p_pocz`) REFERENCES `przystanki` (`ID`),
  ADD CONSTRAINT `bilety_ibfk_4` FOREIGN KEY (`p_konc`) REFERENCES `przystanki` (`ID`);

--
-- Ograniczenia dla tabeli `przystanki`
--
ALTER TABLE `przystanki`
  ADD CONSTRAINT `przystanki_ibfk_1` FOREIGN KEY (`ID_stacji`) REFERENCES `stacje` (`ID`),
  ADD CONSTRAINT `przystanki_ibfk_2` FOREIGN KEY (`ID_trasy`) REFERENCES `trasy` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
